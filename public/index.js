/** working data **/
const tesData = [ {
            title: 'Model S',
            img: 'https://www.tesla.com/tesla_theme/assets/img/models/v1.0/section-hero-background.jpg?20180111',
            copy: 'Model S sets an industry standard for performance and safety. Tesla’s all-electric powertrain delivers unparalleled performance in all weather conditions—with dual motor all-wheel drive, ludicrous acceleration and the highest safety rating of any car ever tested.'
        },
        {
            title: 'Model X',
            img: 'https://www.tesla.com/sites/default/files/images/model-x/section-hero-background.jpg?20180607',
            copy: 'Autopilot advanced safety and convenience features are designed to assist you with the most burdensome parts of driving. Model X comes standard with advanced hardware capable of providing Enhanced Autopilot features today, and full self-driving capabilities in the future.'
        },
        {
            title: 'Model 3',
            img: 'https://cnet1.cbsistatic.com/img/O5SGE9JUnQbLm9tRX51hhQA0ycQ=/724x407/2018/01/26/43b10af3-12d3-432a-b611-31615692b416/2018-tesla-model-3-56-of-161.jpg',
            copy: 'Model S is our flagship, premium sedan with more range, acceleration, displays and customization options. It’s the safest car in its class with unlimited Supercharging for the duration of ownership when referred by an owner.'
        }
    ];

/** working data **/
const jagData = [ {
            title: 'I‑PACE S',
            img: 'https://jaguar.ssl.cdn.sdlmedia.com/636534395410451217LV.jpg?v=19#desktop_1366x650',
            copy: 'The I‑PACE remains true to Jaguar’s tradition of innovation, taking full advantage of electrification to offer dynamic battery-powered performance in a design that is both radical and beautiful.'
        },
        {
            title: 'I‑PACE SE',
            img: 'https://jaguar.ssl.cdn.sdlmedia.com/636426230178770761FG.jpg?v=10#desktop_910x600',
            copy: 'Making the driving experience easier and safer. In addition to the standard features, the Drive Package includes Adaptive Cruise Control with Stop and Go, High-Speed Emergency Braking and Blind Spot Assist.'
        },
        {
            title: 'I‑PACE HSE',
            img: 'https://jaguar.ssl.cdn.sdlmedia.com/636347631573024203WW.jpg?v=14#desktop_1366x769',
            copy: 'The new 10" and 5.5" dual high-deﬁnition touchscreens provide increased ﬂexibility and efficiency of use. View information on one and simultaneously interact with additional features on the other.'
        }
    ];

